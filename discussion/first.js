// console.log('Hi!')


/*
	ES6 updates:

	1. Exponent Operator
		> we use "**" for exponents
		> read as number raised to the power of number
*/

//old
const firstNum = Math.pow(8, 2)
	console.log(firstNum)

//new
const secondNum = 8 ** 2
	console.log (secondNum);

const thirdNum = 5 ** 5
	console.log(thirdNum)

/*
	2. Template Literals
		> allows us to write strings without using the concatination operator (+)
*/


let name = 'George'
//old
//concatination/ Pre-Template Literal

let message = 'Hello ' + name + ' Welcome to Zuitt Coding Boot Camp!'
	console.log ('Message without template literal ' + message)

//new / using template literal / using backticks

message =`Hello ${name}. Welcom to Zuitt Coding Bootcamp`

	console.log(`message with template literal: ${message}`)

let anotherMessage = `
	${name} attended math compittion.
	He won it by solving the problem 8**2 with the solution of ${secondNum}
`
	console.log(anotherMessage)


// operation inside template literal

const interestRate = .1
const principal = 1000

console.log(`The Interest on your savings is: ${principal * interestRate} `)

// array destructuring

/*
	it allows to unpack elements in an array into distinct variables. allows us to name the array elements with variables instead of index number

	syntax:
		let/const [variableName, variableB,] = array;
*/
console.log('')
const fullName = ['Joe', 'Dela', 'Cruz']
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);

// //new
	console.log(`Hello, ${fullName[0]}${fullName[1]}${fullName[2]}! its nice to meet you`)

//array destructuring
const[firstName, middleName, lastName] = fullName

	console.log(firstName)
	console.log(middleName)
	console.log(lastName)

	console.log(`Hello, ${firstName}${middleName}${lastName}! its nice to meet you`)


//Object Destructuring
/*
	allows to unpack properties of objects into distinct variable. Shortens the syntax for accessing properties from objects

	Syntax:
		let/const {propertyName, propertyName,propertyName} = ObjectName
*/
console.log('')
const person = {
	givenName: 'Jane',
	maindenName: 'Dela',
	familyName: 'Cruz'
}

// pre-object destructuring
console.log(person.givenName)
console.log(person.maindenName)
console.log(person.familyName)

function getFullName(givenName, maindenName, familyName){
	console.log(`${givenName} ${maindenName} ${familyName}`)
}
getFullName(person.givenName, person.maindenName, person.familyName);

console.log('')

//using object distructuring
const{maindenName,familyName,givenName} = person
console.log(givenName)
console.log(maindenName)
console.log(familyName)

function getFullName(givenName, maindenName, familyName){
	console.log(`${givenName} ${maindenName} ${familyName}`)
}

getFullName(givenName, maindenName, familyName)

//mini activity

const employees = ['Johnni Walker', 'Smirnoff', 'Hennessy']

const[emp1, emp2, emp3] = employees

	console.log(emp1)
	console.log(emp2)
	console.log(emp3)

	console.log(`Hello, staff welcome ${emp1} ${emp2} ${emp3}! `)

const drink = {
	brand: 'Bacardi',
	type: 'Rum',
	Country: 'Bermuda'
}

const{type,brand,Country} = drink
console.log(brand)
console.log(type)
console.log(Country)

function getAlldrink(brand, type, Country){
	console.log(`lets drink this ${brand} ${type} from ${Country}`)
}

getAlldrink(brand, type, Country)


console.log('')

//pre-arrow function and arrow function

//old //pre-arrow function
/*
	Syntax:
		function functionName(paramA,paramB){
			statement // console.log//return
		}
*/


function printFullname(firstName,middleInitial,lastName){
	console.log(firstName + '' + middleInitial + '' + lastName)
}


printFullname('Rupert ', 'B ', 'Ramos')


//new // arrowfunction
/*
	Syntax:
		let/const variableName = (paramA, paramB) => {
			staetement// console.log// return
		}
*/

const printFullname1 = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`)
}

printFullname1('John Robert', 'A', 'Delavega')

console.log('')
const students = ['yoby', 'emman', 'ronel']

//Function with loop

//pre-arrow function
students.forEach(function(student){
	console.log(student + ' is a studnet')
})

console.log('')
//arrow function
students.forEach((student) => {
	console.log(`${student} is going home`)
})

console.log('')
// implicit return statement

// old//
function add(x, y){
	return x + y
}

let total = add(12,15)
console.log(total)

//arrow//

const addition = (x,y) => x + y

//use return statement when using {}
// const addition = (x, y) => {
// 	return x + y
// }

let total2 = addition(12,15)
console.log(total2)

console.log('')


//default function argument value



const greet = (name = 'User') => {
	return `Good evening, ${name}`
}

console.log(greet())
console.log(greet('Archie'))

/*
	Class-based object blueprint

		> allows creation/instantiation of objects using class as blueprint

		Syntax:
			class className{
				constructor (objectPropertyA, objectPropertyB,) {
				 this.objectpropertyA = objectPropertyA
				 this.objectpropertyB = objectPropertyB
				}
			}
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand
		this.name = name
		this.year = year
	}
}

const myCar = new Car()
console.log(myCar)

myCar.brand = 'Ford'
myCar.name = 'Ranger Raptor'
myCar.year = 2021

console.log(myCar)

const myNewCar = new Car ('Toyota', 'Vios', 2019)
console.log(myNewCar)

// function Car = (brand, name, year) => {
// 	this.brand = brand
// } this can be also used

